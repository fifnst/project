-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 24, 2017 at 11:22 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `absen`
--

-- --------------------------------------------------------

--
-- Table structure for table `jadwal`
--

CREATE TABLE `jadwal` (
  `IDkelas` int(11) NOT NULL,
  `kelas` char(20) DEFAULT NULL,
  `IDpengajar` int(11) DEFAULT NULL,
  `hari` varchar(7) DEFAULT NULL,
  `jam` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal`
--

INSERT INTO `jadwal` (`IDkelas`, `kelas`, `IDpengajar`, `hari`, `jam`) VALUES
(1, 'algoritma', 1, NULL, NULL),
(2, NULL, 4, 'rabu', '09:20:00'),
(3, 'spi', 1, 'senin', NULL),
(4, NULL, 3, 'jumat', '01:35:00'),
(5, NULL, 5, 'rabu', '00:01:00'),
(6, NULL, 4, 'rabu', '14:09:00');

-- --------------------------------------------------------

--
-- Table structure for table `kursus`
--

CREATE TABLE `kursus` (
  `IDkursus` int(11) NOT NULL,
  `IDSiswa` int(11) DEFAULT NULL,
  `IDkelas` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengajar`
--

CREATE TABLE `pengajar` (
  `IDpengajar` int(11) NOT NULL,
  `namapengajar` char(25) DEFAULT NULL,
  `alamat` char(50) DEFAULT NULL,
  `jeniskelamin` varchar(20) NOT NULL,
  `kelas` char(20) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengajar`
--

INSERT INTO `pengajar` (`IDpengajar`, `namapengajar`, `alamat`, `jeniskelamin`, `kelas`, `username`, `password`) VALUES
(1, 'om', 'mns', '0', 'algoritma', 'afif', '1234'),
(2, 'om1', 'mns', '0', 'algoritma', 'afif', '1234'),
(3, 'om2', 'mns', '0', 'algoritma', 'admin', 'admin'),
(4, 'om3', 'mns', 'laki laki', 'algoritma', '', ''),
(5, 'Afif', 'jalan jawa no.101 ujunggading\r\nlembah melintang', 'laki laki', 'algoritma', 'root', 'toor');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `IDsiswa` int(11) NOT NULL,
  `namasiswa` char(25) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `jeniskelamin` char(20) NOT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `kelas` char(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`IDsiswa`, `namasiswa`, `tgl_lahir`, `jeniskelamin`, `alamat`, `kelas`) VALUES
(2, 'afif', '1998-11-01', 'option1', 'adl', 'algoritma'),
(3, 'ahmad', '0001-12-01', 'laki laki', 'cpd', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`IDkelas`),
  ADD KEY `IDpengajar` (`IDpengajar`);

--
-- Indexes for table `kursus`
--
ALTER TABLE `kursus`
  ADD PRIMARY KEY (`IDkursus`),
  ADD KEY `IDSiswa` (`IDSiswa`),
  ADD KEY `IDkelas` (`IDkelas`);

--
-- Indexes for table `pengajar`
--
ALTER TABLE `pengajar`
  ADD PRIMARY KEY (`IDpengajar`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`IDsiswa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jadwal`
--
ALTER TABLE `jadwal`
  MODIFY `IDkelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `kursus`
--
ALTER TABLE `kursus`
  MODIFY `IDkursus` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pengajar`
--
ALTER TABLE `pengajar`
  MODIFY `IDpengajar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `IDsiswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD CONSTRAINT `FK_jadwal_pengajar` FOREIGN KEY (`IDpengajar`) REFERENCES `pengajar` (`IDpengajar`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
