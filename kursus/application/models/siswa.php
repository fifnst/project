<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siswa extends CI_Model {
	
	public function __construct(){

		parent:: __construct();
	}
	public function input_siswa()
	{
		$data=array(
			'namasiswa'		=> $this->input->post('namasiswa'),
			'tgl_lahir'		=> $this->input->post('tgl_lahir'),
			'jeniskelamin'	=> $this->input->post('jeniskelamin'),
			'alamat'		=> $this->input->post('alamat'),
			'kelas'			=> $this->input->post('kelas')
			);

		return $this->db->insert('siswa',$data);


	}

	public function getKelas()
	{
		$query=$this->db->get('jadwal');
		return $query->result_array();
	}
	public function getsiswa()
	{
		$data=$this->db->get('siswa');
		return $data->result_array();

	}

}
