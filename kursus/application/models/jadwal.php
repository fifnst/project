<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends CI_Model {
	
	public function __construct(){

		parent:: __construct();
	}
	public function input_jadwal()
	{
		$data=array(
			'kelas'				=> $this->input->post('kelas'),
			'IDpengajar'		=> $this->input->post('IDpengajar'),
			'hari'				=> $this->input->post('hari'),
			'jam'				=> $this->input->post('jam'),
			);

		return $this->db->insert('jadwal',$data);
	}
	public function getpengajar()
	{
		$data=$this->db->get('pengajar');
		return $data->result_array();

	}



	
}
