<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengajar extends CI_Model {
	
	public function __construct(){

		parent:: __construct();
	}
	public function input_pengajar()
	{
		$data=array(
			'namapengajar'		=> $this->input->post('namapengajar'),
			'jeniskelamin'		=> $this->input->post('jeniskelamin'),
			'alamat'			=> $this->input->post('alamat'),
			'kelas'				=> $this->input->post('kelas'),
			'username'			=> $this->input->post('username'),
			'password'			=> $this->input->post('password'),
			);

		return $this->db->insert('pengajar',$data);


	}

	public function getKelas()
	{
		$query=$this->db->get('jadwal');
		return $query->result_array();
	}

	public function getpengajar()
	{
		$query=$this->db->get('pengajar');
		return $query->result_array();
	}
}
