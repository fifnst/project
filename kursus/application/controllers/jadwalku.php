<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwalku extends CI_Controller {



	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('jadwal');
	}

	public function index()
	{
		$this->load->view('home');

	}
	public function form_siswa()
	{
		$this->load->view('form_siswa');
	}
	public function form_pengajar()
	{
		$this->load->view('form_pengajar');
	}
	public function jadwal()
	{
		$this->load->view('jadwal');
	}

	public function input_jadwal()
	{
		
		$this->jadwal->input_jadwal();
		redirect(base_url('index.php/Home/jadwal?kondisi=sukses'));		
	}

	
}
