<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {


	 public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('siswa');
		$this->load->model('pengajar');
		$this->load->model('jadwal');
	}

	public function index()
	{
		$data['siswa']= $this->siswa->getsiswa();
		$data['pengajar']= $this->pengajar->getpengajar();
		$data['jadwal']= $this->pengajar->getKelas();	
		$this->load->view('home',$data);

	}

	public function form_siswa()
	{
		$data['kelas']= $this->siswa->getKelas();
		$this->load->view('form_siswa',$data);

	}

	public function form_pengajar()
	{
		$data['kelas']= $this->pengajar->getKelas();
		$this->load->view('form_pengajar',$data);
	}

	public function jadwal()
	{
		$data['jadwal']= $this->jadwal->getpengajar();
		$this->load->view('jadwal',$data);
	}

}
