<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Form_pengajar extends CI_Controller {



	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('pengajar');
	}

	public function index()
	{
		$this->load->view('home');

	}
	public function form_siswa()
	{
		$this->load->view('form_siswa');
	}
	public function form_pengajar()
	{
		$this->load->view('form_pengajar');
	}
	public function jadwal()
	{
		$this->load->view('jadwal');
	}

	public function input_pengajar()
	{
		$this->pengajar->input_pengajar();
		redirect(base_url('index.php/Home/form_pengajar?kondisi=sukses'));
		
	}


}
